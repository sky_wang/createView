/**
 * Created by sky_wang on 2017/2/14.
 */

$(document).ready(function() {
    var eGridDiv = document.querySelector('#myGrid');
    var gridOptions;
    var queryAllObj = new Array();
    var queryOpt = new Array();
    var view_item_name = 'default';

    $.get("/show_view/all_views",function(data){
        for (var i =0; i < data.length; i++)
        {
            if (null == data[i]["view_name"])
            {
                continue;
            }
            $("#view_select").append("<option value='"+data[i]["view_name"]+"'>"+data[i]["view_name"]+"</option>");
        }
    });

    $('#view_select').change(function() {
        view_item_name=  $("#view_select").val();
    });

    $("#showView").click(function() {
        if ("default" == view_item_name)
        {
            alert("please select view");
            return;
        }

        $.get("/show_view/view_item", {show_view_name:view_item_name},function(data){
            console.log(data);
            gridOptions = JSON.parse(data["grid_options"]);
            $("#div_search_options").html(data["query_options_html"]);
            queryAllObj = JSON.parse(data["query_options_ids"]);
            new agGrid.Grid(eGridDiv, gridOptions);
        });


    });

    $("#btt_search").click(function() {
        queryOpt = [];
        for (var i =0; i < queryAllObj.length; i++)
        {
            var query_sub_opt = new Object();
            var query_sub_opt_value = new Object();

            var query_id = "#" +  queryAllObj[i]["key"];
            query_sub_opt_value = queryAllObj[i]["value"];
            if (null == $(query_id).val() || '' == $(query_id).val() || 'default' == $(query_id).val())
            {
                continue;
            }
            query_sub_opt["key"]= queryAllObj[i]["key"];
            query_sub_opt_value["value"] = $(query_id).val();
            query_sub_opt["value"] = query_sub_opt_value;
            queryOpt.push(query_sub_opt);
        }

        $.get("/show_view/data_of_view", {show_view_name:view_item_name, query_data:JSON.stringify(queryOpt)},function(data){
            gridOptions.api.setRowData(data);
            gridOptions.api.refresh();
        });
    });

});
