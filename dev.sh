#! /bin/sh
mvn clean compile exec:java \
-Dexec.mainClass=view.App \
-Dpond.web.spi.BaseServer.port=8888 \
-Dpond.debug=true \
-Dpond.debug_classes=pond.web.Pond:pond.db.DB:pond.db.JDBCTpml \
-Dfile.encoding=utf8 
