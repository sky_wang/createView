#auto create view
注意事项：
1、表的id必须规范
2、
{
"view_name":"v_user_role",
"main_table": "t_role",
"columns":[{"key":"role_level","value":"t_role"},
            {"key":"role_name","value":"t_role"},
            {"key":"name","value":"t_user"},
            {"key":"num","value":"t_user"},
            {"key":"sex","value":"t_user"}]}
"query_options_ids":[{"key":"t_user___name",
"value":{"key":"in","value":["wangfeng","gufei"]}},
{"key":"t_user___sex",
"value":{"key":"equal","value":null}}]
"query_options_html":"<div><span>name</span><select id=\"t_user___name\"><option value=\"default\">---请选择---</option><option value=\"wangfeng\"> wangfeng</option><option value=\"gufei\"> gufei</option></select></div><div><span>sex</span><input type=\"text\" id=\"t_user___sex\"></div>"
3、
var data_tables_for_views = [
    {"name":"t_role",
        "value":[{"name":"role_name","value":12}, {"name":"role_level","value":4}]},
    {"name":"t_user",
        "value":[{"name":"num","value":12},{"name":"sex","value":12},{"name":"name","value":12}]}
];