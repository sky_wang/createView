package mode;

import pond.db.Model;

/**
 * Created by sky_wang on 2017/2/14.
 */
public class ViewForGrid extends Model {
    {
        table("view_for_grid");
        id("vid");
        field("view_name");
        field("view_sql");
        field("grid_options");
        field("main_table");
        field("query_options_ids");
        field("query_options_html");
        field("user_defined_options");
    }
}
