package view;

import com.sun.deploy.util.StringUtils;
import mode.ViewForGrid;
import pond.common.FILE;
import pond.common.JSON;
import pond.common.S;
import pond.db.DB;
import pond.db.Record;
import pond.db.connpool.ConnectionPool;
import pond.db.sql.Sql;
import pond.web.InternalMids;
import pond.web.Pond;
import pond.web.Render;
import tools.CreateViewTool;

import java.util.*;

/**
 * Created by sky_wang on 2017/2/5.
 */
public class App {

    public  static final DB DB = new DB(ConnectionPool.c3p0(FILE.loadProperties("db.conf")));

    public  static  void main (String args[]){
        Pond.init(app -> {
            app.get("/*", (req, resp) -> {
                S.echo(req.uri());
                if (req.uri().endsWith("/123"))
                    resp.send(200, "OK");
            });

            app.use("/create_view/*", new CreateView());
            app.use("/show_view/*", new ShowView());
            app.get("/*", app._static("web"));
            app.otherwise(InternalMids.FORCE_CLOSE);
        }).listen(1234);

    }
}
