package tools;

import pond.common.FILE;
import pond.db.DB;
import pond.db.connpool.ConnectionPool;

/**
 * Created by sky_wang on 2017/2/7.
 */
public class TestCreateViewTool {

    public static void main(String args[]) {
        DB db = new DB(ConnectionPool.c3p0(FILE.loadProperties("db.conf")));
        CreateViewTool cvt = new CreateViewTool(db);
        System.out.println("data table json:\r\n"+ cvt.getJsonOfDataTable());

//        [
//        {
//            "name": "t_role",
//                "value": [
//            {
//                "name": "role_level",
//                    "value": 4
//            },
//            {
//                "name": "role_name",
//                    "value": 12
//            }
//            ]
//        },
//        {
//            "name": "t_user",
//                "value": {
//            "name": 12,
//                    "num": 12,
//                    "sex": 12
//        }
//        }
//        ]

        String json =  "{\"view_name\":\"v_user_role\",\"main_table\": \"t_user\", \"columns\":[{\"key\":\"role_level\",\"value\":\"t_role\"},\n" +
                "            {\"key\":\"role_name\",\"value\":\"t_role\"},\n" +
                "            {\"key\":\"name\",\"value\":\"t_user\"},\n" +
                "            {\"key\":\"num\",\"value\":\"t_user\"},{\"key\":\"sex\",\"value\":\"t_user\"}]}";
        System.out.println("view sql:"+json);
        //System.out.println("view sql:\r\n" + cvt.getCreateViewSql(json));
        //db.post(t-> t.exec(cvt.getCreateViewSql(json)));

    }

}
